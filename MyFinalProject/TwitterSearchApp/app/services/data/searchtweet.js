import { client } from "./client";

export const SearchTweet = async (keyword) =>
	client.get(`/tweets/search/recent?query=${keyword}&max_results=30&tweet.fields=author_id,created_at,source,referenced_tweets,public_metrics`, {
		headers: {
			Authorization: `Bearer AAAAAAAAAAAAAAAAAAAAAKboawEAAAAAW0PzI7aeqSSq6Nxkv%2FxeDkzz76k%3DWEOgKoFUBnN0g6viHCg4a9zbST0g8SkxNDXedpdNJcx5PT66vn`,
		},
	});