import { client } from "./client";

export const SearchUsers = async (authorIds) => {
	const authors = authorIds.join(',');
    return client.get(`users?ids=${authors}&user.fields=name,username,profile_image_url,description`, {
		headers: {
			Authorization: `Bearer AAAAAAAAAAAAAAAAAAAAAKboawEAAAAAW0PzI7aeqSSq6Nxkv%2FxeDkzz76k%3DWEOgKoFUBnN0g6viHCg4a9zbST0g8SkxNDXedpdNJcx5PT66vn`,
		},
	})
};