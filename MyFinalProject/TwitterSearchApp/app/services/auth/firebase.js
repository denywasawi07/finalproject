// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyD4wt8alacxTS9gkWRkMXX4Dc3sbjFHDxU",
  authDomain: "auth-finalprojectrn.firebaseapp.com",
  projectId: "auth-finalprojectrn",
  storageBucket: "auth-finalprojectrn.appspot.com",
  messagingSenderId: "800678421215",
  appId: "1:800678421215:web:aea7fd573cc46152a77f6a",
  measurementId: "G-Z71FW0K7LQ"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);