import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	email: '',
	tweetObject: {}
};

export const authSlice = createSlice({
	name: "Auth",
	initialState,
	reducers: {
		login: (state, action) => {
		    state.email = action.payload.email;
		},
		logout: (state) => {
		    state.email = '';
		},
		addSelectedTweet: (state, action) => {
			state.tweetObject = action.payload.tweet;
		},
	},
});

export const { login, logout, addSelectedTweet } = authSlice.actions;
