import { StyleSheet, Text, View, Button, TextInput, TouchableOpacity, StatusBar, Alert, Image } from 'react-native';
import React, {useState} from 'react';
import { useDispatch } from 'react-redux';

import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

import { login } from '../../services/auth/authSlice';

import { AntDesign } from '@expo/vector-icons';

export default function LoginScreen ({navigation}) {
    const firebaseConfig = {
        apiKey: "AIzaSyD4wt8alacxTS9gkWRkMXX4Dc3sbjFHDxU",
        authDomain: "auth-finalprojectrn.firebaseapp.com",
        projectId: "auth-finalprojectrn",
        storageBucket: "auth-finalprojectrn.appspot.com",
        messagingSenderId: "800678421215",
        appId: "1:800678421215:web:aea7fd573cc46152a77f6a",
        measurementId: "G-Z71FW0K7LQ"
      };
      if (!firebase.apps.length){
          const app = firebase.initializeApp(firebaseConfig);
      };

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const dispatch = useDispatch();

    const submit = () => {
        const data = {
            email, password
        };
        console.log(data);
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(()=>{
            dispatch(
                login({email})
            );
        })
        .then(()=>{
            console.log('Login Berhasil');
            navigation.navigate('App');          
        }).catch(()=>{
            console.log('Login Gagal')
        })
    };

    return (
    <View style={styles.container}>
        <View style={{marginBottom: 12}}>
            {/* <AntDesign name="twitter" size={36} color="#1DA1F2"/> */}
            <Image source={require('../../assets/TwitterExplorerIcon.png')} style={{height: 60, resizeMode: 'contain'}}/>
        </View>
        <Text style={styles.textTitle}>Sign In to Twitter Explorer</Text>
        <TextInput 
            style={styles.inputContainer}
            placeholder='Email'
            value={email}
            onChangeText={(value)=>setEmail(value)}
        />
        <TextInput
            style={styles.inputContainer}
            secureTextEntry
            placeholder='Password'
            value={password}
            onChangeText={(value)=>setPassword(value)}
        />
        <TouchableOpacity style={styles.signInButton} onPress={submit}>
            {/* <Button title='LOGIN' onPress={submit}/> */}
            <Text style={styles.textSignIn}>Sign In</Text>
        </TouchableOpacity>
        <View style={{flexDirection: 'row', marginTop: 20}}>
            <Text>Don't have an account? </Text>
            <TouchableOpacity onPress={()=>navigation.navigate('Register')}>
                {/* <Button title='REGISTER' onPress={()=>navigation.navigate('Register')}/> */}
                <Text style={styles.textSignUp}>Sign Up</Text>
            </TouchableOpacity>
        </View>
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
        marginTop: StatusBar.currentHeight,
        flex: 1,
        backgroundColor: '#F5F8FA',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textTitle: {
        fontSize: 20,
        marginBottom: 12,
        color: '#14171A'
    },
    inputContainer: {
        borderRadius: 25,
        width: 300,
        height: 50,
        backgroundColor: '#E1E8ED',
        marginVertical: 4,
        paddingHorizontal: 14
    },
    signInButton: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
        width: 300,
        height: 50,
        backgroundColor: '#14171A',
        marginVertical: 8,
    },
    textSignIn: {
        color: 'white',
        fontSize: 15,
        fontWeight: 'bold'
    },
    textSignUp: {
        color: '#1DA1F2',
        fontWeight: 'bold'
    }
  });