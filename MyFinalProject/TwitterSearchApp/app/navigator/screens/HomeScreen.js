import { StyleSheet, Text, View, Button, TouchableOpacity, TextInput, FlatList, Image } from 'react-native';
import React, { useEffect, useState } from 'react';
import { SearchTweet } from '../../services/data/searchtweet';
import { SearchUsers } from '../../services/data/searchuser';
import {StatusBar} from 'react-native';

import { FontAwesome } from '@expo/vector-icons'; 
import { useSelector, useDispatch } from 'react-redux';
import { AntDesign } from '@expo/vector-icons';

import { addSelectedTweet } from '../../services/auth/authSlice';

export default function HomeScreen({navigation}) {
  const [keyword, setKeyword] = useState('');
  const [dataTweets, setDataTweets] = useState([]);
  const dispatch = useDispatch();
  
  const handleError = (err) => {
    console.warn("Error Status: ", err);
  };

  const getTweets = async () => {
  try {
    setDataTweets([]);
    const res1 = await SearchTweet(keyword);
    const arrTweets = res1.data.data;
          
    const userIds = arrTweets.map((e)=>e.author_id);
    const res2 = await SearchUsers(userIds);
    const arrUsers = res2.data.data;

    const ObjectUsers = {};
    arrUsers.map((e)=>{
        ObjectUsers[e.id] = e;
    });

    for(let i in arrTweets){
        arrTweets[i].name = ObjectUsers[arrTweets[i].author_id].name;
        arrTweets[i].username = ObjectUsers[arrTweets[i].author_id].username; 
        arrTweets[i].profile_image_url = ObjectUsers[arrTweets[i].author_id].profile_image_url;
        arrTweets[i].description = ObjectUsers[arrTweets[i].author_id].description;

    };
    setDataTweets(arrTweets);
  } catch (error) {
    handleError(error);
  }
  };

  const selectTweet = (tweet) => {
    dispatch(
      addSelectedTweet({tweet})
    );
    navigation.navigate('Detail');
  }

  useEffect(()=>{
      console.log("userInfo: ", dataTweets);
  }, [dataTweets])
  
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.textHeader}>Twitter Explorer</Text>
        <View style={styles.searchContainer}>
          <View style={{flex: 1, justifyContent: 'center'}}>
              <TextInput
                  style={styles.searchInput}
                  placeholder='search recent tweets'
                  value={keyword}
                  onChangeText={(value)=>setKeyword(value)}/>
          </View>
          <TouchableOpacity onPress={getTweets} style={styles.searchIcon}>
            <FontAwesome name="search" size={20} color="#657786"/>
          </TouchableOpacity>
          {/* <Button title='Search' onPress={getTweets}/> */}
        </View>
      </View>
      <FlatList
        data={dataTweets}
        keyExtractor={(item)=>item.id}
        renderItem={({ item })=>{
          let tweetDate = new Date(item.created_at)
          let hours = tweetDate.getHours()
          if (hours < 10) hours = '0' + hours
          let minutes = tweetDate.getMinutes()
          if (minutes < 10) minutes = '0' + minutes
          let date = tweetDate.getDate()
          const arrMonth = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des']
          let month = arrMonth[tweetDate.getMonth()]
          
          let time = `${date} ${month}, ${hours}:${minutes}`
          return (
              <TouchableOpacity style={styles.tweetContainer} onPress={()=>selectTweet(item)}>
                <View>
                  <Image source={{uri: item.profile_image_url}} style={{height: 50, width: 50, borderRadius: 25}}/> 
                </View>
                <View style={styles.bodyTweet}>
                  <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={{flex: 1}}>  
                      <Text style={{fontWeight: 'bold', flexWrap: 'wrap'}}>{item.name}</Text>
                    </View>
                    <Text style={{fontSize: 12, color: '#657786', justifyContent: 'center'}}>{time}</Text>
                  </View>
                  <Text style={{fontStyle: 'italic'}}>@{item.username}</Text>
                  <Text>{item.text}</Text>
                </View>
              </TouchableOpacity>
          )
        }}
      />
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
      marginTop: StatusBar.currentHeight,
      flex: 1,
      backgroundColor: '#F5F8FA',
      alignItems: 'center',
      justifyContent: 'flex-start',
    },
    header: {
      alignItems: 'flex-start',
      borderBottomWidth: 0.3,
      borderColor: '#1DA1F2',
      backgroundColor: '#657786'
    },
    textHeader: {
      marginTop: 10,
      marginLeft: 14,
      fontSize: 18,
      fontWeight: 'bold',
      color: '#F5F8FA',
      alignSelf: 'flex-start',
    },
    searchContainer: {
      flexDirection: 'row',
      height: 40,
      borderRadius: 20,
      backgroundColor: '#F5F8FA',
      marginHorizontal: 8,
      marginVertical: 12,
      paddingHorizontal: 12,
      borderWidth: 0.2,
      borderColor: '#1DA1F2'
    },
    searchInput: {
      color: '#14171A'
    },
    searchIcon: {
      justifyContent: 'center',
      marginRight: 2
    },
    tweetContainer: {
      flexDirection: 'row',
      borderBottomWidth: 0.4,
      borderColor: '#1DA1F2',
      paddingHorizontal: 8,
      paddingVertical: 8,
      backgroundColor: '#F5F8FA',
      
    },
    bodyTweet: {
      marginHorizontal: 8,
      width: 280
    }
  });