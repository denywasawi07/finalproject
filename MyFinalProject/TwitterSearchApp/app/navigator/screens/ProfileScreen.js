import { StyleSheet, Text, View, Button, TouchableOpacity, StatusBar } from 'react-native';
import React, {useState, useEffect} from 'react';
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

import { useSelector, useDispatch } from 'react-redux';
import { logout } from '../../services/auth/authSlice';
import { AboutText } from '../../services/data/AboutText';

export default function ProfileScreen({navigation}) {
  const dispatch = useDispatch();
  const email = useSelector((state)=>state.Auth.email);
  
  const submit = () => {
    firebase.auth().signOut()
    .then(()=>{
        dispatch(
          logout()
        )
    })
    .then(()=>{
        console.log('Logout Berhasil');
        navigation.navigate('Login');
    })
  };
  
    return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>About Twitter Explorer</Text>
      </View>
      <View style={styles.contentContainer}>
        <View style={{marginVertical: 6}}>
          <Text style={{fontSize: 16}}>Hello, {email}</Text>
        </View>
        <View style={{marginVertical: 1}}>
          <Text style={{textAlign: 'justify'}}>{AboutText}</Text>
        </View>
        <View style={{marginVertical: 1}}>
          <Text>That's it! Hit me up on my twitter: @denywasawi</Text>
        </View>
      </View>
      <TouchableOpacity style={styles.signOutButton} onPress={submit}>
        <Text style={styles.textSignOut}>Sign Out</Text>
      </TouchableOpacity>
      <View style={{position: 'absolute', bottom: 10}}>
        <Text style={{fontSize: 11}}>(Jabar Coding Camp · Reac Native Class · 2022)</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: StatusBar.currentHeight,
    flex: 1,
    backgroundColor: '#F5F8FA',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerContainer: {
    marginBottom: 24
  },
  contentContainer: {
    // borderWidth: 1,
    marginHorizontal: 28,
  },
  signOutButton: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    width: 140,
    height: 50,
    backgroundColor: '#14171A',
    marginTop: 24,
  },
  textSignOut: {
    color: 'white',
    fontSize: 15,
    fontWeight: 'bold'
},
  });