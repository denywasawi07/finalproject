import { StyleSheet, Text, View, Button, TouchableOpacity, StatusBar, Image } from 'react-native';
import React from 'react';

import { AntDesign } from '@expo/vector-icons';

export default function StartScreen ({navigation}) {
  return (
    <View style={styles.container}>
      <View style={{marginBottom: 12}}>
        {/* <AntDesign name="twitter" size={42} color="#1DA1F2"/> */}
        <Image source={require('../../assets/TwitterExplorerIcon.png')} style={{height: 100, resizeMode: 'contain'}}/>
      </View>
      <Text style={styles.textTitle}>Welcome to Twitter Explorer</Text>
      <TouchableOpacity style={styles.continueButton} onPress={()=>navigation.navigate('Login')}>
        {/* <Button title='NEXT' onPress={()=>navigation.navigate('Login')}/> */}
        <Text style={styles.textContinue}>Continue</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: StatusBar.currentHeight,
    flex: 1,
    backgroundColor: '#F5F8FA',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTitle: {
    fontSize: 20,
    marginBottom: 12,
    color: '#14171A'
  },
  continueButton: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    width: 100,
    height: 50,
    backgroundColor: '#14171A',
    marginVertical: 8,
  },
  textContinue: {
    color: 'white',
    fontSize: 15,
    fontWeight: 'bold'
  }
});