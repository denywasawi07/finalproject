import { StyleSheet, Text, View, Button, StatusBar, Image, ScrollView } from 'react-native';
import React, {useEffect, useState} from 'react';
import { useSelector } from 'react-redux';

import { LookUpTweet } from '../../services/data/lookuptweet';
import { SearchUsers } from '../../services/data/searchuser';

export default function DetailScreen() {
  const [tweetSource, setTweetSource] = useState({})
  
  const tweet = useSelector((state)=>state.Auth.tweetObject);
  console.log("tweet info: ", tweet)

  let tweetDate = new Date(tweet.created_at);
  let hours = tweetDate.getHours();
    if (hours < 10) hours = '0' + hours;
  let minutes = tweetDate.getMinutes();
    if (minutes < 10) minutes = '0' + minutes;
  let date = tweetDate.getDate();
  const arrMonth = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];
  let month = arrMonth[tweetDate.getMonth()];
  let year = tweetDate.getFullYear();

  let reply = tweet.public_metrics.reply_count;
  let rewteet = tweet.public_metrics.retweet_count;
  let like = tweet.public_metrics.like_count;
  let quote = tweet.public_metrics.quote_count;

  const handleError = (err) => {
    console.warn("Error Status: ", err);
  };

  const lookup = async (TweetID) => {
    try {
      const res = await LookUpTweet(TweetID)
      const name = res.data.includes.users[0].name;
      console.log("tweet name: ", name)

      const username = '@' + res.data.includes.users[0].username;
      console.log("tweet username: ", username)

      const textTweet = res.data.data[0].text;
      console.log("tweet: ", textTweet);

      const sourceID = res.data.data[0].author_id;
      const resUser = await SearchUsers([sourceID]);

      const profilePict = resUser.data.data[0].profile_image_url;


      const sourceTweet = {name, username, textTweet, profilePict};
      setTweetSource(sourceTweet);

    } catch (error){
      handleError(error)
    }
  }

  useEffect(() => {
    if (tweet.referenced_tweets){
      const ID = tweet.referenced_tweets[0].id;
      console.log(ID)
      lookup(ID);
    }
  }, [])
  
  if (!tweet.referenced_tweets){
    return (
      <View style={styles.container}>
        <View style={styles.profileContainer}>
          <View>
            <Image source={{uri: tweet.profile_image_url}} style={{height: 60, width: 60, borderRadius: 30}}/>
          </View>
          <View style={styles.nameContainer}>
            <Text style={{fontWeight: 'bold', flexWrap: 'wrap'}}>{tweet.name}</Text>
            <Text style={{fontStyle: 'italic'}}>@{tweet.username}</Text>
          </View>
        </View>
        <View style={styles.tweetContainer}>
          <Text>{tweet.text}</Text>
        </View>
        <View style={styles.detailContainer}>
          <Text style={{color: '#657786'}}>{hours}:{minutes} · {month} {date}, {year} · {tweet.source}</Text>
        </View>
        <View style={styles.retweetContainer}>
          <Text style={{color: '#657786'}}>{reply} Reply · {rewteet} Retweet · {quote} Quote Retweet · {like} Like</Text>
        </View>
      </View>
    )
  } else if (tweet.referenced_tweets[0].type === 'retweeted'){
      return (
        <View style={styles.container}>
          <View style={styles.profileContainer}>
            <View>
              <Image source={{uri: tweet.profile_image_url}} style={{height: 60, width: 60, borderRadius: 30}}/>
            </View>
            <View style={styles.nameContainer}>
              <Text style={{fontWeight: 'bold', flexWrap: 'wrap'}}>{tweet.name}</Text>
              <Text style={{fontStyle: 'italic'}}>@{tweet.username}</Text>
            </View>
          </View>
          <View style={styles.tweetContainer}>
            <Text>{tweet.text}</Text>
          </View>
          <View style={styles.detailContainer}>
            <Text style={{color: '#657786'}}>{hours}:{minutes} · {month} {date}, {year} · {tweet.source}</Text>
          </View>
          <View style={styles.retweetContainer}>
            <Text style={{color: '#657786'}}>{reply} Reply · {rewteet} Retweet · {quote} Quote Retweet · {like} Like</Text>
          </View>
          
          <View style={styles.subHeader}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>Retweeting:</Text>
          </View>
          <ScrollView>
            <View style={styles.sourceContainer}>
              <View style={styles.profileContainer}>
                <View>
                  <Image source={{uri: tweetSource.profilePict}} style={{height: 50, width: 50, borderRadius: 25}}/>
                </View>
                <View style={styles.nameContainer}>
                  <Text style={{fontWeight: 'bold', flexWrap: 'wrap'}}>{tweetSource.name}</Text>
                  <Text style={{fontStyle: 'italic'}}>{tweetSource.username}</Text>
                </View>
              </View>
              <View style={styles.tweetContainer}>
                <Text>{tweetSource.textTweet}</Text>
              </View>
            </View>
          </ScrollView>
        </View>
      )
  } else if (tweet.referenced_tweets[0].type === 'quoted'){
      return (
        <View style={styles.container}>
          <View style={styles.profileContainer}>
            <View>
              <Image source={{uri: tweet.profile_image_url}} style={{height: 60, width: 60, borderRadius: 30}}/>
            </View>
            <View style={styles.nameContainer}>
              <Text style={{fontWeight: 'bold', flexWrap: 'wrap'}}>{tweet.name}</Text>
              <Text style={{fontStyle: 'italic'}}>@{tweet.username}</Text>
            </View>
          </View>
          <View style={styles.tweetContainer}>
            <Text>{tweet.text}</Text>
          </View>
          <View style={styles.detailContainer}>
            <Text style={{color: '#657786'}}>{hours}:{minutes} · {month} {date}, {year} · {tweet.source}</Text>
          </View>
          <View style={styles.retweetContainer}>
            <Text style={{color: '#657786'}}>{reply} Reply · {rewteet} Retweet · {quote} Quote Retweet · {like} Like</Text>
          </View>
          
          <View style={styles.subHeader}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>Quoting:</Text>
          </View>
          <ScrollView>
            <View style={styles.sourceContainer}>
              <View style={styles.profileContainer}>
                <View>
                  <Image source={{uri: tweetSource.profilePict}} style={{height: 50, width: 50, borderRadius: 25}}/>
                </View>
                <View style={styles.nameContainer}>
                  <Text style={{fontWeight: 'bold', flexWrap: 'wrap'}}>{tweetSource.name}</Text>
                  <Text style={{fontStyle: 'italic'}}>{tweetSource.username}</Text>
                </View>
              </View>
              <View style={styles.tweetContainer}>
                <Text>{tweetSource.textTweet}</Text>
              </View>
            </View>
          </ScrollView>
        </View>
      )
  } else if (tweet.referenced_tweets[0].type === 'replied_to'){
      return (
        <View style={styles.container}>
          <View style={styles.profileContainer}>
            <View>
              <Image source={{uri: tweet.profile_image_url}} style={{height: 60, width: 60, borderRadius: 30}}/>
            </View>
            <View style={styles.nameContainer}>
              <Text style={{fontWeight: 'bold', flexWrap: 'wrap'}}>{tweet.name}</Text>
              <Text style={{fontStyle: 'italic'}}>@{tweet.username}</Text>
            </View>
          </View>
          <View style={styles.tweetContainer}>
            <Text>{tweet.text}</Text>
          </View>
          <View style={styles.detailContainer}>
            <Text style={{color: '#657786'}}>{hours}:{minutes} · {month} {date}, {year} · {tweet.source}</Text>
          </View><View style={styles.retweetContainer}>
            <Text style={{color: '#657786'}}>{reply} Reply · {rewteet} Retweet · {quote} Quote Retweet · {like} Like</Text>
          </View>
          
          <View style={styles.subHeader}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>Replying:</Text>
          </View>
          <ScrollView>
            <View style={styles.sourceContainer}>
              <View style={styles.profileContainer}>
                <View>
                  <Image source={{uri: tweetSource.profilePict}} style={{height: 50, width: 50, borderRadius: 25}}/>
                </View>
                <View style={styles.nameContainer}>
                  <Text style={{fontWeight: 'bold', flexWrap: 'wrap'}}>{tweetSource.name}</Text>
                  <Text style={{fontStyle: 'italic'}}>{tweetSource.username}</Text>
                </View>
              </View>
              <View style={styles.tweetContainer}>
                <Text>{tweetSource.textTweet}</Text>
              </View>
            </View>
          </ScrollView>
        </View>
      )
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#F5F8FA',
      alignItems: 'stretch',
      justifyContent: 'flex-start',
    },
    profileContainer: {
      // borderWidth: 1,
      flexDirection: 'row',
      marginTop: 10,
      marginHorizontal: 10,
      marginVertical: 2,
      alignItems: 'center'
    },
    nameContainer: {
      // borderWidth: 1,
      marginHorizontal: 10,
    },
    tweetContainer: {
      // borderWidth: 1,
      marginHorizontal: 10,
      marginVertical: 2,
      paddingBottom: 2,
    },
    detailContainer: {
      // borderWidth: 1,
      borderColor: '#1DA1F2',
      borderTopWidth: 0.4,
      borderBottomWidth: 0.4,
      marginHorizontal: 10,
      paddingVertical: 8,
    },
    retweetContainer: {
      // borderWidth: 1,
      borderColor: '#1DA1F2',
      borderBottomWidth: 0.4,
      marginHorizontal: 10,
      paddingVertical: 8,
    },
    subHeader: {
      // borderWidth: 1,
      marginTop: 18,
      marginBottom: 6,
      marginHorizontal: 10
    },
    sourceContainer: {
      borderWidth: 0.4,
      borderRadius: 10,
      borderColor: '#1DA1F2',
      marginLeft: 14,
      marginRight: 10,
      marginBottom: 10,
      paddingVertical: 2
    }
  });