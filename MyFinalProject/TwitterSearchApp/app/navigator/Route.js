import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { AntDesign } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import StartScreen from './screens/StartScreen';
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import HomeScreen from './screens/HomeScreen';
import ProfileScreen from './screens/ProfileScreen';
import DetailScreen from './screens/DetailScreen';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

export default function Route() {
    return (
        <Stack.Navigator screenOptions={{animation: 'fade', headerShown: false}}>
            <Stack.Screen name='Start'component={StartScreen}/>
            <Stack.Screen name='Login'component={LoginScreen}/>
            <Stack.Screen name='Register'component={RegisterScreen}/>
            <Stack.Screen name='App'component={MainApp}/>
        </Stack.Navigator>
    )
}

const MainApp = () => (
    <Tab.Navigator
        initialRouteName='Explore'
        screenOptions={{animation: 'fade', headerShown: false, tabBarActiveTintColor: '#14171A', tabBarActiveBackgroundColor: '#E1E8ED', tabBarInactiveBackgroundColor: '#F5F8FA'}}>
            <Tab.Screen options={{tabBarIcon:()=>(<MaterialCommunityIcons name="text-search" size={24} color="black" />)}} name='Explore' component={Explore}/>
            <Tab.Screen options={{tabBarIcon:()=>(<AntDesign name="profile" size={24} color="black" />)}} name='About' component={ProfileScreen}/>
    </Tab.Navigator>
)

const Explore = () => (
    <Stack.Navigator>
        <Stack.Screen options={{headerShown: false}} name='Home'component={HomeScreen}/>
        <Stack.Screen options={{title: 'Tweets'}} name='Detail'component={DetailScreen}/>
    </Stack.Navigator>
)