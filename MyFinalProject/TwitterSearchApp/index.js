import React from 'react';

import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';

import { store } from './app/store'
import Route from './app/navigator/Route';

export default function TwitterSearchApp() {
  return (
    <Provider store={store}>
      <NavigationContainer>
          <Route />
      </NavigationContainer>
    </Provider>
  )
}