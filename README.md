# Twitter Explorer
Jabar Coding Camp - React Native Class Final Project
## About
Aplikasi ini merupakan platform pencarian tweet. Aplikasi dapat menampilkan hasil pencarian terkini dengan menggunakan kata kunci yang dimasukkan oleh pengguna. Tweet hasil pencarian dapat dipilih dan dilihat informasi lebih detailnya seperti jumlah reply, retweet, like, tweet yang di-retweet, quote, atau reply.

## Sumber Data
API: Twitter API